#include <conio.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

struct Accesoriu {
    char nume[15];
    char brand[15];
    int pret;

    void citireDate() {
        fflush(stdin);
        cout << "\nIntrodu numele accesoriului: ";
        cin >> nume;
        cout << "Introdu brand-ul accesoriului: ";
        cin >> brand;
        cout << "Introdu pretul accesoriului: ";
        cin >> pret;
        cout << "\n";
    }

    void afisareDate() {
        cout << "\nNumele accesoriului este:" << nume << endl;
        cout << "Brand-ul accesoriului este:" << brand << endl;
        cout << "Pretul accesoriului este:" << pret << endl;
    }
};

int main() {
    int n;
    cout << "Introduceti numarul de accesorii: ";
    cin >> n;

    Accesoriu *accesoriu = new Accesoriu[n];
    for (int i = 0; i < n; i++) {
        accesoriu[i].citireDate();
    }

    cout << "Afisare resultat:" << endl;

    for (int i = 0; i < n; i++) {
        accesoriu[i].afisareDate();
    }

    getch();
    return 0;
}
