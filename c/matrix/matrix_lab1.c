#include <stdio.h>

int main() {
    const int MAX = 4;
    int M[MAX][MAX] = {{-1, 2, 3, 4}, {-5, 6, 7, 8}, {-9, 1, 2, 3}, {-4, 5, 6, 7}};

    int suma = 0;
    int produsul = 1;

    int i, j;
    for (i = 0; i < MAX; i++) {
        for (j = 0; j < MAX; j++) {
            // Calculeaza suma si produsul pentru elementele de sub diagonala
            // principala, inclusiv si de pe diagonala.
            if (j <= i) {
                if (M[i][j] < 0) {
                    suma = suma + M[i][j];
                } else {
                    produsul = produsul * M[i][j];
                }
            }

            // Calculeaza suma si produsule pentru elementele de pe diagonala
            // secundara, dar numai din partea de sus a diagonalei.
            if (j > i && j == MAX - i - 1) {
                if (M[i][j] < 0) {
                    suma = suma + M[i][j];
                } else {
                    produsul = produsul * M[i][j];
                }
            }
        }
    }

    printf("Suma elementelor: %d \n", suma);
    printf("Produsul elementelor: %d", produsul);
    
    return 0;
}