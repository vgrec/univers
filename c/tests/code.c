#include <stdio.h>
#include <unistd.h>
#include <conio.h>
#include <windows.h> // WinApi header

void delay(int millis)
{
    usleep(millis * 1000);
}

void printMessage(char s[])
{
    int i;
    for (i = 0; i < strlen(s); i++)
    {
        printf("%c", s[i]);
        delay(50);
    }
    delay(5000);
}

/**
* http://web.theurbanpenguin.com/adding-color-to-your-output-from-c/
*/
void setTextColorRed()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 12);
}

void setTextColorGreen()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 10);
}

void resetTextColor()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 7);
}

void clrscr()
{
    system("@cls||clear");
}

void showLoginSuccessMessage()
{
    clrscr();

    setTextColorGreen();
    int i;
    for (i = 0; i < 3; i++)
    {
        printf("\n");
        printf("  ACCESS GRANTED!");
        sleep(1);
        clrscr();
        sleep(1);
    }
}

int main()
{

    sleep(1);
    setTextColorGreen();
    printf("\n\n");
    printMessage("  TOP SECRET INFORMATION\n");
    printMessage("  This information is not meant to be publicily disclosed.\n");
    printMessage("  CIA, 2020\n\n");
    sleep(1);

    resetTextColor();
    printf("  Press ENTER to continue: \n");

    resetTextColor();
    getch();

    clrscr();
    printf("\n\n");
    printf("  Login: \n");
    setTextColorRed();
    printf("\n");
    printMessage("  Username: AgEnTXaker911\n");
    sleep(1);
    printMessage("  Password: **************");

    resetTextColor();
    printf("\n\n");
    printf("  Press ENTER to continue: \n");
    getch();

    showLoginSuccessMessage();
    printf("\n\n");
    printMessage("  Congratulations on finishing with success the first University Year!!! :))");

    resetTextColor();
    printf("\n\n");
    printf("  Press ENTER to start summer vacations! \n");
    getch();

    return 0;
}
