#include <stdio.h>
#include <stdlib.h>

int main(void) {
    printf("\nIntroduceti elementele listei.");
    printf("\n - Dupa fiecare numar apasati tasta ENTER.");
    printf("\n - Pentru a finisa procesul culegeti 0 (zero) si apasati ENTER\n");

    int input = 1;
    while (input != 0) {
        printf("> ");
        scanf("%d", &input);
    }

    return 0;
}