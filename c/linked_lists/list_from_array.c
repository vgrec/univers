#include <stdio.h>
#include <stdlib.h>
// Shift + Option + F

struct Node {
    int data;
    struct Node* next;
};

int main(void) {
    const int N = 5;
    int A[N] = {1, 2, 3, 4, 5, 6};

    struct Node* current = NULL;
    struct Node* head = NULL;
    for (int i = 0; i < N; i++) {
        if (current == NULL) {
            current = (struct Node*)malloc(sizeof(struct Node));
            current->data = A[i];
            head = current;
        }else {
            struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
            new_node->data = A[i];
            new_node->next = NULL;
            
            current->next = new_node;
            current = new_node;
        }
    }



    struct Node* node = head;
    while (node != NULL) {
        printf("Element %d\n", node->data);
        node = node->next;
    }

    printf("Finish");

    return 0;
}
