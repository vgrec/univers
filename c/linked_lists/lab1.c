#include <stdio.h>
#include <stdlib.h>
// Shift + Option + F

struct Node {
    int data;
    struct Node* next;
};

void printList(struct Node* head) {
    struct Node* node = head;
    while (node != NULL) {
        printf("%d, ", node->data);
        node = node->next;
    }
}

struct Node* buildList() {
    printf("\nIntroduceti elementele listei.");
    printf("\n - Dupa fiecare numar apasati tasta ENTER.");
    printf("\n - Pentru a finisa, introduceti 0 (zero) si apasati ENTER\n");

    struct Node* current = NULL;
    struct Node* head = NULL;

    int element = 1;
    while (element != 0) {
        printf("> ");
        scanf("%d", &element);

        struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
        new_node->data = element;
        new_node->next = NULL;

        if (head == NULL) {
            head = new_node;
            current = head;
        } else {
            current->next = new_node;
            current = new_node;
        }
    }

    return head;
}

struct Node* deletePositiveNumbers(struct Node* head) {
    struct Node start, *current;
    start.next = head;

    current = &start;
    while (current->next != NULL) {
        struct Node* nextNode = current->next;
        if (nextNode->data >= 0) {
            current->next = nextNode->next;
            free(nextNode);
        } else {
            current = current->next;
        }
    }

    return start.next;
}

struct Node* deleteOddNumbers(struct Node* head) {
    struct Node start, *current;
    start.next = head;

    current = &start;
    while (current->next != NULL) {
        if (current->next->data % 2 != 0) {
            current->next = current->next->next;
        } else {
            current = current->next;
        }
    }

    return start.next;
}

int main(void) {
    struct Node* originalList = buildList();

    printf("\nLista originala: ");
    printList(originalList);

    printf("\nLista fara numere positive: ");
    struct Node* negativeNumbers = deletePositiveNumbers(originalList);
    printList(negativeNumbers);

    printf("\nLista fara numere impare: ");
    struct Node* evenNumbers = deleteOddNumbers(negativeNumbers);
    printList(evenNumbers);

    return 0;
}
