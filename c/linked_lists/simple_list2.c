#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int i;

    struct IntrariLista {
        int numar;
        struct IntrariLista *urmator;
    };

    struct IntrariLista start, *nod;
    start.urmator = NULL;
    
    nod = &start;

    for (i = 1; i <= 10; i++) {
        nod->urmator =
            (struct IntrariLista *)malloc(sizeof(struct IntrariLista));
        nod = nod->urmator;
        nod->numar = i;
        nod->urmator = NULL;
    }

    nod = start.urmator;
    while (nod) {
        printf("%d ", nod->numar);
        nod = nod->urmator;
    }

    return 0;
}