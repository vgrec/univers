#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};

void pair(struct Node* head) {
    struct Node* first = head;
    struct Node* second = head;
    second = second->next;
    printf("\n");
    while (first != NULL) {
        while (second != NULL) {
            printf("(%d , %d)\n", first->data, second->data);
            second = second->next;
        }
        first = first->next;
        second = first->next;
        if (second == NULL) return;
    }
}

int size(struct Node* head_ref) {
    struct Node* curr = head_ref;
    int sz = 0;
    while (curr != NULL) {
        curr = curr->next;
        sz++;
    }
    return sz;
}

void rotate(struct Node** head_ref, int N, int sz) {
    N = N % sz;
    N = sz - N;
    if (N == 0) return;
    struct Node* current = *head_ref;
    int count = 1;
    while (count < N && current != NULL) {
        current = current->next;
        count++;
    }
    if (current == NULL) return;
    struct Node* NthNode = current;
    while (current->next != NULL) current = current->next;
    current->next = *head_ref;
    (*head_ref)->prev = current;
    *head_ref = NthNode->next;
    (*head_ref)->prev = NULL;
    NthNode->next = NULL;
}

void push(struct Node** head_ref, int new_data) {
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->data = new_data;
    new_node->prev = NULL;
    new_node->next = (*head_ref);
    if ((*head_ref) != NULL) (*head_ref)->prev = new_node;
    *head_ref = new_node;
}

void printList(struct Node* node) {
    while (node->next != NULL) {
        printf("%d ", node->data);
        node = node->next;
    }
    printf("%d ", node->data);
}

int main() {
    printf("\nIntroduceti elementele listei.");
    printf("\n - Dupa fiecare numar apasati tasta ENTER.");
    printf("\n - Pentru a finisa, introduceti 0 (zero) si apasati ENTER\n");

    struct Node* head = NULL;

    int element = 1;
    while (element != 0) {
        printf("> ");
        scanf("%d", &element);

        push(&head, element);
    }

    int b;
    printf("\nIntroduceti b: ");
    scanf("%d", &b);

    int sz = size(head);
    printf("Lista este \n");
    printList(head);

    pair(head);
    rotate(&head, b, sz);
    printf("Lista rotita este : \n");
    printList(head);

    return 0;
}