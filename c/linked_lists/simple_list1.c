#include <stdio.h>
#include <stdlib.h>
// Shift + Option + F

struct Node {
    int data;
    struct Node* next;
};

int main(void) {
    struct Node* first = (struct Node*)malloc(sizeof(struct Node));
    struct Node* second = (struct Node*)malloc(sizeof(struct Node));
    struct Node* third = (struct Node*)malloc(sizeof(struct Node));
    struct Node* fourth = (struct Node*)malloc(sizeof(struct Node));

    first->data = 1;
    first->next = second;

    second->data = 2;
    second->next = third;

    third->data = 3;
    third->next = fourth;

    fourth->data = 4;
    fourth->next = NULL;

    struct Node* node = first;
    node->next = node->next->next;
    node = node->next;
    node->next = node->next->next;

    struct Node* current = first;
    while (current != NULL) {
        printf("Element %d\n", current->data);
        current = current->next;
    }

    printf("Finish");

    return 0;
}
